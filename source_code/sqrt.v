// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 101
// 2527455: Moritz Willig
// 2940980: Maximilian Weller
// ----------------
//
// Description:
// ----------------
// elevator square root calculation
//
// Version History:
// ----------------
// 140116:

`timescale 1ns / 1ns

module sqrt

          (input wire         CLK,
           input wire         RESET,

           input wire         NEXT,
           input wire         PREVIOUS,

	   output reg 	      DONE,
	   output reg  [31:0] SQRT);
  
/* =============================INSERT CODE HERE======================================*/ 

//vars from the given algorithm
reg [31:0] n;
reg [31:0] L;
reg [31:0] H;
reg [31:0] delta;

reg [31:0] norm; //square of approximated numbers 32.32
reg [63:0] normTemp; //square of approximated numbers 16.16

//constants for 16.16 fixed point numbers
localparam F1616_ONE  = 1<<16;
localparam F1616_HALF = F1616_ONE>>1;
localparam DELTA_ST   = F1616_HALF;

always @(posedge CLK) begin
	if (RESET) begin
		//initial setup
		delta=F1616_HALF;
		SQRT=F1616_ONE;
		n=F1616_ONE;
		DONE=1;
	end else if (NEXT) begin
		H=SQRT+DELTA_ST;
		L=SQRT;
		n=n+F1616_ONE;
		delta=F1616_HALF;
		DONE=0;
	end else if (PREVIOUS) begin
		H=SQRT;
		L=SQRT-DELTA_ST;
		n=n-F1616_ONE;
		delta=F1616_HALF;
		DONE=0;
	end else begin
		//bisect "function"
		if (!DONE)  begin
			if (delta!=0) begin
				//set new iteration average (L+H)/2
				SQRT=((L+H)>>1);
				
				//create correct fixed point multiplication
				/*
					creates 32.32 bit number
					only 16.16 number is relevant. Over- or underflows
					are ignored because they can not be represented by an
					output anyway.
				*/
				normTemp=(SQRT*SQRT);
				norm=normTemp[47:16];
				
				//adjust higher or lower limit
				if (norm>n) begin
					H=SQRT;
					//L=L;
					//n=n;
				end else begin
					//H=H;
					L=SQRT;
					//n=n
				end;
				delta=delta>>1;
			end else begin
				DONE=1;
			end
		end
	end
end

/* ====================================================================================*/

endmodule 
