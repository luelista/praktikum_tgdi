// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 101
// 2527455: Moritz Willig
// 2940980: Maximilian Weller
// ----------------
//
// Description:
// ----------------
// elevator user-control
//
// Version History:
// ----------------
// 140116: set parameter FLOORS to 30
// 140116: set parameter FLOOR_BITS to 5

`timescale 1ns / 1ns

module user_ctrl

          #(parameter FLOORS     = 30,
                      FLOOR_BITS =  5)


          (input  wire                    CLK,
           input  wire                    RESET,
           
           input  wire [(FLOOR_BITS-1):0] CURRENT_FLOOR_IN,           // cabin stage
           input  wire                    HALTED,
           input  wire [(FLOORS-1)    :0] FLOOR_REQUEST,              // floor button pressed
           input  wire                    MANUAL_DOOR_CLOSE_IN,       // close button pressed  
           input  wire                    MANUAL_DOOR_OPEN_IN,        // open button pressed         
           input  wire                    MANUAL_ALARM_IN,            // alarm button pressed

           output wire [(FLOOR_BITS-1):0] CURRENT_FLOOR_OUT,          // forward to cabin display
           output wire                    MANUAL_DOOR_CLOSE_OUT,      // door close cmd
           output wire                    MANUAL_DOOR_OPEN_OUT,       // door open cmd
           output wire                    MANUAL_ALARM_OUT,           // user alarm         
           output wire [(FLOORS-1)    :0] DESTINATIONS,               // destinations
           output reg  [(FLOOR_BITS-1):0] CLEAR_FLOOR_BUTTON,         // reset_button
           output reg                     CLEAR_FLOOR_BUTTON_VALID);  // validate reset_button

/* =============================INSERT CODE HERE======================================*/ 

// Durchreichen der Zielwahl, des akt. Stockwerks
assign DESTINATIONS = FLOOR_REQUEST;
assign CURRENT_FLOOR_OUT = CURRENT_FLOOR_IN;

// Durchreichen der Manuellen Taster
assign MANUAL_DOOR_CLOSE_OUT = MANUAL_DOOR_CLOSE_IN;
assign MANUAL_DOOR_OPEN_OUT = MANUAL_DOOR_OPEN_IN;
assign MANUAL_ALARM_OUT = MANUAL_ALARM_IN;

// Rückstellen der Beleuchtung der Zielwahltasten bei Halt im Stockwerk
always @ (posedge CLK) begin
	if (HALTED)
		CLEAR_FLOOR_BUTTON <= CURRENT_FLOOR_IN;
	
	// Rückstellangabe nur gültig, wenn angehalten
	CLEAR_FLOOR_BUTTON_VALID <= HALTED;
end;


/* ====================================================================================*/

endmodule
