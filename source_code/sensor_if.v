// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 101
// 2527455: Moritz Willig
// 2940980: Maximilian Weller
// ----------------
//
// Description:
// ----------------
// elevator sensor-interface
//
// Version History:
// ----------------
// 140116: changed unit in comment of VELOCITY_THRESHOLD

`timescale 1ns / 1ns

module sensor_if

          #(parameter WEIGHT_BITS        =   10,  // weight sensor input bits
                      MAX_WEIGHT         =  500,  // maximum weight [kg]
                      VELOCITY_BITS      =   11,  // velocity sensor input bits
                      VELOCITY_THRESHOLD = 2000)  // velocity threshold [cm/s]


          (input  wire                          CLK,
           input  wire                          RESET,

           input  wire                          DOOR_LIGHT_CURTAIN_IN,         // 1 if object in the way
           input  wire [(VELOCITY_BITS-1)   :0] VELOCITY_SENSOR_IN,            // speed [cm/s]
           input  wire [(WEIGHT_BITS-1)     :0] WEIGHT_SENSOR_IN,              // weight [kg]
           input  wire                          SMOKE_PARTICLE_SENSOR_IN,      // 1 if smoke detected
           
           output reg                           DOOR_LIGHT_CURTAIN_OUT,        // 0 if door is free
           output reg                           VELOCITY_SENSOR_OUT,           // 1 if speed is ok
           output reg                           WEIGHT_SENSOR_OUT,             // 1 if weight is ok
           output reg                           SMOKE_PARTICLE_SENSOR_OUT);    // 0 if smoke is ok

/* =============================INSERT CODE HERE======================================*/ 
	
	// Z�hler f�r Minimaldauer
   reg [7:0]                                    dlc_counter;
   reg [7:0]                                    vs_counter;
   reg [7:0]                                    ws_counter;
   reg [7:0]                                    sps_counter;
   
   
	 always @(posedge CLK) begin
		// R�ckstellen aller Z�hler bei RESET
      if (RESET) begin
         dlc_counter = 0;
         vs_counter = 0;
         ws_counter = 0;
         sps_counter = 0;
      end

      // Falls Lichtvorhang unterbrochen, entspr. Z�hler inkrementieren, sonst Z�hler r�ckstellen
      if (DOOR_LIGHT_CURTAIN_IN) dlc_counter = dlc_counter + 1;
      else dlc_counter = 0;
		
		// Falls Geschwindigkeit �berschritten, entspr. Z�hler inkrementieren, sonst Z�hler r�ckstellen
      if (VELOCITY_SENSOR_IN >= VELOCITY_THRESHOLD) vs_counter = vs_counter + 1;
      else vs_counter = 0;

		// Falls Gewicht �berschritten, entspr. Z�hler inkrementieren, sonst Z�hler r�ckstellen
      if (WEIGHT_SENSOR_IN >= MAX_WEIGHT) ws_counter = ws_counter + 1;
      else ws_counter = 0;

		// Falls Rauchpartikel detektiert, entspr. Z�hler inkrementieren, sonst Z�hler r�ckstellen
      if (SMOKE_PARTICLE_SENSOR_IN) sps_counter = sps_counter + 1;
      else sps_counter = 0;
      
		// Ausangswerte setzen, wenn Z�hler Minimaldauer erreicht hat
	   DOOR_LIGHT_CURTAIN_OUT = (dlc_counter == 255);
      VELOCITY_SENSOR_OUT = (vs_counter != 255);
      WEIGHT_SENSOR_OUT = (ws_counter != 255);
      SMOKE_PARTICLE_SENSOR_OUT = (sps_counter == 255);
   end
   
	 
/* ====================================================================================*/

endmodule
