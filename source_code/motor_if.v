// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 101
// 2527455: Moritz Willig
// 2940980: Maximilian Weller
// ----------------
//
// Description:
// ----------------
// elevator motor-interface (door and elevator)
//
// Version History:
// ----------------
// 140116: added � to corresponding comment-lines
// 140116: deleted parameter MOTOR_STEP_BITS2

`timescale 1ns / 1ns

module motor_if

         #(parameter INITIAL_POSITION  =       0,      // inital position is at floor 0
                     MOTOR_STEP        =      50,      // [�m] (a single motor step is 50�m = 0,05mm)
                     CONTROL_STEP      =      10,      // [mm] (a control step is 10mm = 1cm)
                     DISTANCE_BITS     =      14,      // max distance 12m = 12.000 mm
                     CONTROL_STEP_BITS =      11,      // 12m <> 1.200 control steps
                     MOTOR_STEP_BITS   =      18,      // max distance 12m = 240.000 * 50�m
                     MAX_POSITION_R    =    1200,      // [control steps] (1.200 control steps = 240.000 motor steps = 12 m)
                     MAX_POSITION_L    =       0,      // [control steps] (0 control steps = 0 motor steps = 0 m)
                     DELAY_COUNT_BITS  =      17,      // 17 bits required to count up to C_0 = 100.000
                     C_0               =  100000,      // initial counter value <> 100 Hz @ 10 MHz clock
                     C_MIN             =     250)      // minimum counter value <> 40 kHz @ 10 MHz clock

         (input  wire                      CLK,
          input  wire                      RESET,

          input  wire [DISTANCE_BITS-1 :0] DISTANCE,        // levels [mm] / door_width [mm]
          input  wire                      ROTATE_LEFT,     // open or down
          input  wire                      ROTATE_RIGHT,    // close or up
          
          output reg                       CTRL_STEP_DONE,  // 10mm distance is a step
          output reg                       DONE,            // motor request executed
          output wire                      A,               // motor coil a output
          output wire                      B,               // motor coil a output
          output wire                      C,               // motor coil a output
          output wire                      D);              // motor coil a output

/* =============================INSERT CODE HERE======================================*/ 

localparam MM_TO_UM=1000; //conversion ratio from mm to �m
localparam MM_TO_MS=1000/MOTOR_STEP; //motor steps per millimeter
localparam CS_TO_MS=(CONTROL_STEP*MM_TO_UM)/MOTOR_STEP; //control steps to motor steps

localparam MinVel=C_0;
localparam MaxVel=C_MIN;

reg [4-1:0] motorState; //represents ABCD

//save rotate left/right-values
reg rLeft;
reg rRight;

//sqrt module
wire sqDone;
wire sqNext;
wire sqPrev;
wire [31:0] sqrtVal;
reg [31:0] lastSqrtVal;
wire sqReset;
sqrt sqrtV(CLK,sqReset,
   sqNext,sqPrev,sqDone,
	sqrtVal
);

/*
motor pattern (next step by rotating)
  ABCD
 1x  x
 2xx
 3 xx
 4  xx
*/
function [4-1:0] rotateRight; input [4-1:0] in; rotateRight={in[0:0],in[3:1]}; endfunction
function [4-1:0] rotateLeft ; input [4-1:0] in; rotateLeft ={in[2:0],in[3:3]}; endfunction

//fsm states
localparam ST_IDLE=0;
localparam ST_ACCEL=1;
localparam ST_RUN=2;
localparam ST_DECEL=3;

localparam STR_IDLE=0;
localparam STR_ROTATE=1;
localparam STR_WAIT=2;

localparam STC_IDLE=0;
localparam STC_WAIT=1;
localparam STC_CALC=2;

reg [1:0] status,statusNext; //status for acceleration
reg [1:0] statusRot, statusRotNext; //status for motor rotation
reg [1:0] statusCalc, statusCalcNext; //status for idle time calculations


//motor positions (Curr=current, Dest=Destination)
reg [MOTOR_STEP_BITS-1:0] CurrDist;
reg [MOTOR_STEP_BITS-1:0] CurrPos ;
reg [MOTOR_STEP_BITS-1:0] DestDist;
reg [MOTOR_STEP_BITS-1:0] DestPos ;
reg [MOTOR_STEP_BITS-1:0] LastDist;
reg [MOTOR_STEP_BITS-1:0] LastPos ;
reg [MOTOR_STEP_BITS-1:0] CurrPosCS; //currentpos mod CS

//calculation registers
reg [DELAY_COUNT_BITS-1:0] CurrWait; //ticks until next step
reg [DELAY_COUNT_BITS-1:0] LastWait; //ticks of current step
reg [DELAY_COUNT_BITS-1:0] nextDelay; //delay for next step
reg [DELAY_COUNT_BITS+16-1:0] nextDelayTemp; //DELAY_COUNT_BITS.16 fixedpoint needed for delay calculations
reg [DELAY_COUNT_BITS+32-1:0] nextDelayTempExt; //DELAY_COUNT_BITS.32 fixedpoint needed for delay calculations
reg needCalc; //1=calculation needed

reg [MOTOR_STEP_BITS-1:0] AccelDist; //distance accelerated

always @(posedge CLK, posedge RESET) begin
	if (RESET) begin //reset status to idle and positions to inital
		status<=ST_IDLE;
		statusRot<=STC_IDLE;
		statusCalc<=STC_IDLE;
		
		rLeft=0;
		rRight=0;
		motorState=4'b1001;
		lastSqrtVal={16'b1,16'b0}; //save 1 as 16.16fix
		
		CurrPos=INITIAL_POSITION*CS_TO_MS; //interpret INIT_POS as CS -> https://moodle.informatik.tu-darmstadt.de/mod/forum/discuss.php?d=26872
		CurrDist=0;
		CurrPosCS=0;
		DestDist=CurrDist;
		DestPos =CurrPos;
		LastDist=CurrDist;
		LastPos =CurrPos;
		
		CurrWait=0;
		LastWait=0;
		nextDelay=MinVel;
		needCalc=0;
		
		AccelDist=0;
	end else begin //set next state
		status<=statusNext;
		statusRot<=statusRotNext;
		statusCalc<=statusCalcNext;
	end
end

always @(*) begin
	statusNext    =status;
	statusRotNext =statusRot;
	statusCalcNext=statusCalc;
	
	//next state logic
	case (status) //see diagram in documentation
		ST_IDLE: begin //accel if destination pos changed
			CurrWait=0;
			LastWait=0;
			if (DestPos-CurrPos!=0)
				statusNext=ST_ACCEL;
		end
		ST_ACCEL: begin
			if (CurrDist==DestDist>>1)
				statusNext=ST_DECEL;
			else
				if (LastWait==MaxVel)
					statusNext=ST_RUN;
		end
		ST_RUN: begin
			if (CurrDist==DestDist-20) //AccelDist)
				statusNext=ST_DECEL;
		end
		ST_DECEL: begin
			if (CurrPos==DestPos)
				begin
					statusNext=ST_IDLE;
					statusRotNext=STR_IDLE;
					CurrWait=0;
					LastWait=0;
				end
		end
	endcase
	
	//idle (until wait!=0)->wait (until delay=0)->rotate (return to idle)
	case (statusRot)
		STR_IDLE: begin
			if (CurrWait!=0)
				statusRotNext=STR_WAIT;
		end
		STR_ROTATE: begin
			if (CurrWait==0)
				statusRotNext=STR_IDLE;
			else
				statusRotNext=STR_WAIT;
		end
		STR_WAIT: begin
			if (CurrWait==1)
				statusRotNext=STR_ROTATE;
		end
	endcase

	case (statusCalc) //idle (wait for calcNeeded)->wait (wait for sqrt module)->calc(return to idle)
		STC_IDLE: begin
			if (needCalc)
				statusCalcNext=STC_WAIT;
		end
		STC_WAIT: begin
			if (sqDone)
				statusCalcNext=STC_CALC;
		end
		STC_CALC: begin
			statusCalcNext=STC_IDLE;
		end
	endcase
end

//logic
always @(posedge CLK) begin
	needCalc=0;
	
	//distance change while not idle
	if ((status!=ST_IDLE) && (LastDist!=DISTANCE*MM_TO_MS)) begin
		//if (statusNext!=ST_IDLE) begin
			LastPos =CurrPos;
			LastDist=DISTANCE*MM_TO_MS;
			
			CurrDist=0;
			CurrPosCS=0;
		//end
		
		//recalculate new destination distance & position
		if (rLeft) begin
			if (((MAX_POSITION_L*CS_TO_MS*MM_TO_UM)+DISTANCE*MM_TO_UM)>CurrPos*MOTOR_STEP) begin //in micrometers
				DestDist=CurrPos-(MAX_POSITION_L*CS_TO_MS); //limit reached, set remaining distance
			end else begin
				DestDist=DISTANCE*MM_TO_MS;
			end
		end
			
		if (rRight) begin
			if (DISTANCE*MM_TO_UM+CurrPos*MOTOR_STEP>MAX_POSITION_R*CONTROL_STEP*MM_TO_UM) begin //in micrometers
				DestDist=(MAX_POSITION_R*CS_TO_MS)-CurrPos; //limit reached, set remaining distance
			end else begin
				DestDist=DISTANCE*MM_TO_MS;
			end
		end
		
		//update DestPos
		if (ROTATE_LEFT ) DestPos=CurrPos-DestDist;
		if (ROTATE_RIGHT) DestPos=CurrPos+DestDist;
	end
	
	case (status)
		ST_IDLE: begin
			if (statusNext==ST_IDLE) begin
				rLeft =ROTATE_LEFT;
				rRight=ROTATE_RIGHT;
			end else begin
				LastPos =CurrPos;
				LastDist=DISTANCE*MM_TO_MS;
			end
			
			if (rLeft) begin
				if (((MAX_POSITION_L*CS_TO_MS*MM_TO_UM)+DISTANCE*MM_TO_UM)>CurrPos*MOTOR_STEP) begin //in micrometers
					DestDist=CurrPos-(MAX_POSITION_L*CS_TO_MS);
				end else begin
					DestDist=DISTANCE*MM_TO_MS;
				end
			end
			
			if (rRight) begin
				if (DISTANCE*MM_TO_UM+CurrPos*MOTOR_STEP>MAX_POSITION_R*CONTROL_STEP*MM_TO_UM) begin //in micrometers
					DestDist=(MAX_POSITION_R*CS_TO_MS)-CurrPos;
				end else begin
					DestDist=DISTANCE*MM_TO_MS;
				end
			end
			
			if (ROTATE_LEFT ) DestPos=CurrPos-DestDist;
			if (ROTATE_RIGHT) DestPos=CurrPos+DestDist;
			
			
			CurrDist=0;
			CurrPosCS=0;
			AccelDist=0;
		end
		ST_ACCEL: begin
			if (CurrWait==0) begin //last step finished
				//setup new calculation
				needCalc=1;
				
				//trigger new step
				CurrWait=nextDelay;
				LastWait=nextDelay; //save value
				
				AccelDist=AccelDist+1;
			end
		end
		ST_RUN: begin
			if (statusRot==STR_IDLE)
				CurrWait=nextDelay;
		end
		ST_DECEL: begin
			if (CurrWait==0) begin //last step finished
				//setup new calculation
				needCalc=1;
				
				//trigger new step
				CurrWait=nextDelay;
			end
		end
	endcase
	
	//rotation
	case (statusRot)
		STR_ROTATE: begin
			//if (CurrWait==0) begin
				if (rRight) begin
					motorState=rotateRight(motorState);
					CurrPos=CurrPos+1;
					
					CurrDist=CurrDist+1;
					CurrPosCS=CurrPosCS+1;
				end
				
				if (rLeft) begin
					motorState=rotateLeft (motorState);
					CurrPos=CurrPos-1;
					
					CurrDist=CurrDist+1;
					CurrPosCS=CurrPosCS+1;
				end
			//end
		end
		STR_WAIT: begin
			CurrWait=CurrWait-1;
		end
	endcase
	
	//delay calc
	case (statusCalc)
		STC_WAIT: begin
		end
		STC_CALC: begin
			//nextDelayTemp=(sqrtVal-lastSqrtVal);
			//$display("SQRT: %f",nextDelayTemp[31:16]+$bitstoreal({1'b0,11'b01111111111,{nextDelayTemp[15:0],36'h000000000}})-1);
			
			//difference between sqrt
			if (status==ST_ACCEL)
				nextDelayTemp=(sqrtVal-lastSqrtVal);
			else
				nextDelayTemp=(lastSqrtVal-sqrtVal);
			
			//if Temp=0=sqrt-lastSqrt
			if (nextDelayTemp==0)
				nextDelayTemp={16'b1,16'b0};
			
			//{C_0,16'b0} = DELAY_COUNT_BITS.16 fixed point value
			nextDelayTempExt={C_0,16'b0}*nextDelayTemp;
			//nextDelayTempExt=DELAY_COUNT_BITS.32 fixed point
			
			//nextDelayTemp=DELAY_COUNT_BITS
			nextDelay=nextDelayTempExt[DELAY_COUNT_BITS+32-1:33]; //fixedp. to unsigned int [DELAY_COUNT_BITS-1:0]
			//$display("next delay: %d",nextDelay);
			
			//check if limits reached
			if (nextDelay<=MaxVel) begin
				nextDelay=MaxVel;
			end
			
			if (nextDelay>=MinVel) begin
				nextDelay=MinVel;
			end
			//$display("applied delay: %d",nextDelay);
			
			nextDelay=MaxVel;
			
			//save sqrtVal for later calculations
			lastSqrtVal=sqrtVal;
		end
	endcase
	
	//assign DONE & CONTROL_STEP_DONE
	DONE=(statusNext==ST_IDLE);
	if (CurrPosCS==(1*CS_TO_MS)) begin
		CTRL_STEP_DONE=1;
		CurrPosCS=0;
	end else begin
		CTRL_STEP_DONE=0;
	end
end

	
assign {A,B,C,D}=motorState;
//assign DONE=(statusNext==ST_IDLE);
//assign CTRL_STEP_DONE=(CurrPosCS==1*CS_TO_MS);


assign sqNext=(sqDone==1)&&(needCalc)&&(status==ST_ACCEL)&&(statusCalc!=STC_WAIT);
assign sqPrev=(sqDone==1)&&(needCalc)&&(status==ST_DECEL)&&(statusCalc!=STC_WAIT);
assign sqReset=RESET;
/* ====================================================================================*/
   
endmodule 
