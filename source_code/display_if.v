// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 101
// 2527455: Moritz Willig
// 2940980: Maximilian Weller
// ----------------
//
// Description:
// ----------------
// elevator display-interface
//
// Version History:
// ----------------
// 140116:

`timescale 1ns / 1ns

module display_if

          #(parameter FLOOR_BITS       =  7,
                      DISPLAY_SEGMENTS = 14)


          (input  wire [(FLOOR_BITS-1)      :0] CURRENT_FLOOR,

           output wire [(DISPLAY_SEGMENTS-1):0] ENABLE_SEGMENT);  // enable    

/* =============================INSERT CODE HERE======================================*/ 

   // xxxxsplit number to tens and ones (bcd representation)
	
	//--> define registers for bcd repr.
	reg[3:0] tens; //assign tens = CURRENT_FLOOR / 10;
	reg[3:0] ones; //assign ones = CURRENT_FLOOR % 10;
	
	//--> define register for ENABLE_SEGMENT to allow assignment in always
	reg [(DISPLAY_SEGMENTS-1):0] q;
	
	always @ (CURRENT_FLOOR) begin
		//--> split number to tens and ones (bcd representation)
		// there might be a smarter way....
		if (CURRENT_FLOOR >= 'd90) begin
		  tens = 'd9; ones = CURRENT_FLOOR - 'd90;
		end else if (CURRENT_FLOOR >= 'd80) begin
		  tens = 'd8; ones = CURRENT_FLOOR - 'd80;
		end else if (CURRENT_FLOOR >= 'd70) begin
		  tens = 'd7; ones = CURRENT_FLOOR - 'd70;
		end else if (CURRENT_FLOOR >= 'd60) begin
		  tens = 'd6; ones = CURRENT_FLOOR - 'd60;
		end else if (CURRENT_FLOOR >= 'd50) begin
		  tens = 'd5; ones = CURRENT_FLOOR - 'd50;
		end else if (CURRENT_FLOOR >= 'd40) begin
		  tens = 'd4; ones = CURRENT_FLOOR - 'd40;
		end else if (CURRENT_FLOOR >= 'd30) begin
		  tens = 'd3; ones = CURRENT_FLOOR - 'd30;
		end else if (CURRENT_FLOOR >= 'd20) begin
		  tens = 'd2; ones = CURRENT_FLOOR - 'd20;
		end else if (CURRENT_FLOOR >= 'd10) begin
		  tens = 'd1; ones = CURRENT_FLOOR - 'd10;
		end else begin
		  tens = 'd0;
		  ones = CURRENT_FLOOR;
		end
	
	
		//--> convert tens to 7-seg repr.
		case (tens)
			0: q[13:7] = 7'b1111110;
			1: q[13:7] = 7'b0110000;
			2: q[13:7] = 7'b1101101;
			3: q[13:7] = 7'b1111001;
			4: q[13:7] = 7'b0110011;
			5: q[13:7] = 7'b1011011;
			6: q[13:7] = 7'b1011111;
			7: q[13:7] = 7'b1110000;
			8: q[13:7] = 7'b1111111;
			9: q[13:7] = 7'b1111011;
			default: q[13:7] = 7'b1111111;
		endcase
		
		//--> convert ones to 7-seg repr.
		case (ones)
			0: q[6:0] = 7'b1111110;
			1: q[6:0] = 7'b0110000;
			2: q[6:0] = 7'b1101101;
			3: q[6:0] = 7'b1111001;
			4: q[6:0] = 7'b0110011;
			5: q[6:0] = 7'b1011011;
			6: q[6:0] = 7'b1011111;
			7: q[6:0] = 7'b1110000;
			8: q[6:0] = 7'b1111111;
			9: q[6:0] = 7'b1111011;
			default: q[6:0] = 7'b1111111;
		endcase
	end
	
	//--> copy register to output wire
	assign ENABLE_SEGMENT = q;
 
/* ====================================================================================*/
  
endmodule
