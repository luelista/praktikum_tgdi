// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 101
// 2527455: Moritz Willig
// 2940980: Maximilian Weller
// ----------------
//
// Description:
// ----------------
// elevator floor-request-interface 
//
// Version History:
// ----------------
// 140116: set parameter FLOORS to 30
// 140116: set parameter FLOOR_BITS to 5
// 140116: changed comment of ENLIGHT_BUTTONS

`timescale 1ns / 1ns

module floor_request_if

          #(parameter FLOORS     = 30,
                      FLOOR_BITS =  5)


          (input  wire                          CLK,
           input  wire                          RESET,    

           input  wire [(FLOOR_BITS-1)      :0] CURRENT_FLOOR,
           input  wire                          HALTED,
           
           output wire [(FLOORS*2)-1:0]         ENLIGHT_BUTTONS, //only (FLOORS-1)*2 bits would be needed, but easier to handle
           
           input  wire [(FLOORS*2)-1:0]         FLOOR_REQUEST_IN,
           output wire [(FLOORS*2)-1:0]         FLOOR_REQUEST_OUT); 
      
/* =============================INSERT CODE HERE======================================*/ 
	
	// store all the requests
	reg [(FLOORS*2)-1:0] requests;
	
	always @(posedge CLK) begin
		// reset requests
		if (RESET) requests = 0;
		
		// wenn der Aufzug an einer Station h�lt, entsprechende Requests (up und down) l�schen
		if (HALTED) begin
			requests = requests & ~( 'b11 << ((CURRENT_FLOOR << 1)) );
		end
		
		// neue Requests speichern
		requests = requests | FLOOR_REQUEST_IN;
		
	end
	
	// requests weiterreichen an steuerung und button-leds
	assign FLOOR_REQUEST_OUT = requests;
	assign ENLIGHT_BUTTONS = requests;
	
/* ====================================================================================*/

endmodule
