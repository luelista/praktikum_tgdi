// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 101
// 2527455: Moritz Willig
// 2940980: Maximilian Weller
// ----------------
//
// Description:
// ----------------
// elevator user-interface
//
// Version History:
// ----------------
// 140116: set parameter FLOORS to 30
// 140116: set parameter FLOOR_BITS to 5

`timescale 1ns / 1ns

module user_interface
             #(parameter FLOORS           = 30,
                         FLOOR_BITS       =  5,
                         DISPLAY_SEGMENTS = 14)
                      
              (
               input wire               	    CLK,
               input wire                           RESET,
               
               // INPUTS FROM CTRL UNIT                                                     		
               input  wire [(FLOOR_BITS-1)      :0] CURRENT_FLOOR,           
               input  wire                          HALTED,
               // INPUTS FROM BUTTONS
               input  wire [(FLOORS-1)          :0] FLOOR_SELECT,               // REQUESTS FROM CABIN                                  
               input  wire                          CLOSE_DOOR,  
					input  wire                          OPEN_DOOR,			
               input  wire                          PASSENGER_ALARM,                   		
               input  wire [(FLOORS*2)-1        :0] FLOOR_REQUEST,              // REQUESTS FROM FLOORS
                                   
               output wire [(FLOORS-1)          :0] ENLIGHT_BUTTONS,            // ENLIGHT ELEVATOR BUTTONS
               output wire [(FLOORS*2)-1        :0] ENLIGHT_FLOOR_BUTTONS,      // ENLIGHT FLOOR REQUEST BUTTONS
               output wire [(DISPLAY_SEGMENTS-1):0] ENABLE_SEGMENT,             // SEGMENT DISPLAY
               // OUTPUTS FOR CTRL UNIT
               output wire [(FLOOR_BITS-1)      :0] NEXT_FLOOR,			// DETERMINE NEXT FLOOR
					output wire [(FLOOR_BITS-1)      :0] CURRENT_FLOOR_OUT);               

/* =============================INSERT CODE HERE======================================*/ 

	// Keyboard Interface <-> User Control
	wire open,close,alarm, clear_btn_valid;
	wire [(FLOORS-1)          :0] cabin_floor_rq;
	wire [(FLOOR_BITS-1)      :0] clear_btn;
	
	// User Control -> nirvana
	wire open_nul, close_nul, alarm_nul;
	
	// User Control -> Display Interface
	wire [(FLOOR_BITS-1)      :0] current_display_floor;
	
	// User Control -> Next Floor Ctrl
	wire [(FLOORS-1)          :0] destinations;
	
	// Floor Request Interface -> Next Floor Ctrl
	wire [(FLOORS*2)-1        :0] outside_floor_rq;
	
	user_ctrl #(
              .FLOORS     (FLOORS),
              .FLOOR_BITS (FLOOR_BITS))
              
      USER_CTRL_i (
					    //INPUT
                  .CLK(CLK),
                  .RESET(RESET),
                  .CURRENT_FLOOR_IN(CURRENT_FLOOR),
                  .HALTED(HALTED),                           
                  .FLOOR_REQUEST(cabin_floor_rq),  //<--keyboard_if
                  .MANUAL_DOOR_CLOSE_IN(close),  	//<--keyboard_if
						.MANUAL_DOOR_OPEN_IN(open),  		//<--keyboard_if
                  .MANUAL_ALARM_IN(alarm),  			//<--keyboard_if
						
                  //OUTPUT
                  .CURRENT_FLOOR_OUT(current_display_floor), //-->display_if  
                  .MANUAL_DOOR_CLOSE_OUT(close_nul),  //-->???
                  .MANUAL_DOOR_OPEN_OUT(open_nul),  	//-->???
                  .MANUAL_ALARM_OUT(alarm_nul),  		//-->???
                  .DESTINATIONS(destinations),  		//-->next_floor_ctrl
                  .CLEAR_FLOOR_BUTTON(clear_btn),  	//-->keyboard_if
                  .CLEAR_FLOOR_BUTTON_VALID(clear_btn_valid));    //-->keyboard_if   
	
	keyboard_if #(
               .FLOORS     (FLOORS),
               .FLOOR_BITS (FLOOR_BITS))
               
      KEYBOARD_IF_i (
					    //INPUT
                    .CLK(CLK),
                    .RESET(RESET),
                    .FLOOR_SELECT(FLOOR_SELECT),
                    .CLOSE_DOOR_IN(CLOSE_DOOR),
                    .OPEN_DOOR_IN(OPEN_DOOR),
                    .PASSENGER_ALARM_IN(PASSENGER_ALARM),
                    .CLEAR_FLOOR_BUTTON(clear_btn),    //<-- user ctrl
                    .CLEAR_FLOOR_BUTTON_VALID(clear_btn_valid),   //<-- user ctrl
						  
						  //OUTPUT
                    .CLOSE_DOOR_OUT(close),    //--> user ctrl
						  .OPEN_DOOR_OUT(open),    //--> user ctrl
                    .PASSENGER_ALARM_OUT(alarm),    //--> user ctrl
                    .SELECTED_FLOORS(cabin_floor_rq),    //--> user ctrl
                    .ENLIGHT_BUTTONS(ENLIGHT_BUTTONS));

  display_if #(
              .FLOOR_BITS       (FLOOR_BITS),
              .DISPLAY_SEGMENTS (DISPLAY_SEGMENTS))     
              
      DISPLAY_IF_i (            
					    //INPUT
                   .CURRENT_FLOOR  (current_display_floor),   //<-- user ctrl
						 //OUTPUT
                   .ENABLE_SEGMENT (ENABLE_SEGMENT));


  floor_request_if #(
                    .FLOORS     (FLOORS), 
                    .FLOOR_BITS (FLOOR_BITS)) 
  
      FLOOR_REQUEST_IF_i (
                         .CLK(CLK),
                         .RESET(RESET),
			 
                         .CURRENT_FLOOR(CURRENT_FLOOR),
                         .HALTED(HALTED),
                         
                         .ENLIGHT_BUTTONS(ENLIGHT_FLOOR_BUTTONS),
                         
                         .FLOOR_REQUEST_IN(FLOOR_REQUEST),
                         .FLOOR_REQUEST_OUT(outside_floor_rq)); //-->next_floor_ctrl
                
  next_floor_ctrl #(
                   .FLOORS     (FLOORS), 
                   .FLOOR_BITS (FLOOR_BITS)) 
  
      NEXT_FLOOR_CTRL_i (
                        .CLK(CLK),
                        .RESET(RESET),
                        
                        .DESTINATIONS(destinations),   //<--user_ctrl<--keyboard_if
                        .FLOOR_REQUEST(outside_floor_rq), //<--floor_request_if
                        .HALTED(HALTED),
                        .CURRENT_FLOOR(CURRENT_FLOOR),

                        .NEXT_FLOOR(NEXT_FLOOR));

	
/* ====================================================================================*/
  
endmodule
