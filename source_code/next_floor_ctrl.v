// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 101
// 2527455: Moritz Willig
// 2940980: Maximilian Weller
// ----------------
//
// Description:
// ----------------
// elevator next-floor-control
//
// Version History:
// ----------------
// 140116: set parameter FLOORS to 30
// 140116: set parameter FLOOR_BITS to 5

`timescale 1ns / 1ns

module next_floor_ctrl

          #(parameter FLOORS     = 30,
                      FLOOR_BITS =  5)


          (input  wire                          CLK,
           input  wire                          RESET,

           input  wire                          HALTED,
           input  wire [(FLOORS-1)          :0] DESTINATIONS,
           input  wire [((FLOORS*2)-1)      :0] FLOOR_REQUEST,
           input  wire [(FLOOR_BITS-1)      :0] CURRENT_FLOOR,
           
           output reg  [(FLOOR_BITS-1)      :0] NEXT_FLOOR);
             
/* =============================INSERT CODE HERE======================================*/ 

reg direction; //0=down 1=up
//helper macros
localparam DIR_UP = 1;  
localparam DIR_DOWN = 0;

//reg is set to 1 if the control is searching for next_floor
reg searching;
reg [(FLOOR_BITS-1):0] checkFloor;

// Speichern der letzten Werte der Eingabeparameter, um auf
// �nderungen reagieren zu k�nnen
reg [(FLOOR_BITS-1)          :0] last_CURRENT_FLOOR;
reg [(FLOORS-1)          :0] last_DESTINATIONS;
reg [((FLOORS*2)-1)      :0] last_FLOOR_REQUEST;

always @(posedge CLK) begin
	// R�ckstellen aller Parameter bei RESET
	if (RESET) begin
		direction=DIR_DOWN;
		searching<=1;
		NEXT_FLOOR<=0;
		last_CURRENT_FLOOR <= 0;
		last_DESTINATIONS <= 0;
		last_FLOOR_REQUEST <= 0;
		
	// Pr�fen auf ge�nderte Eingabeparameter
	end else if ((CURRENT_FLOOR != last_CURRENT_FLOOR) || (DESTINATIONS != last_DESTINATIONS) || (FLOOR_REQUEST != last_FLOOR_REQUEST)) begin
		// Speichern der neuen letzten Eingabeparameter
		last_CURRENT_FLOOR <= CURRENT_FLOOR;
		last_DESTINATIONS <= DESTINATIONS;
		last_FLOOR_REQUEST <= FLOOR_REQUEST;
		
		// Suchauftrag ativieren
		searching <= 1;
		
		//$display("      some request was issued - starting to search next floor! (current floor=%d, dir=%d)", CURRENT_FLOOR, direction);
		
		if ((CURRENT_FLOOR != 0) && (CURRENT_FLOOR != FLOORS-1)) begin
			// N�chstes zu pr�fendes Stockwerk anhand gespeicherter Richtung
			checkFloor=CURRENT_FLOOR+(direction?1:-1);
		end else begin
			// Wenn an Ober- oder Untergrenze angekommen, Richtung wechseln
			if (CURRENT_FLOOR==0) begin
				checkFloor<=0;
				direction=DIR_DOWN;
			end else begin
				checkFloor<=FLOORS - 1;
				direction=DIR_UP;
			end;
		end
	
	
	// Wenn sich keine Eingabeparameter ge�ndert haben UND ein Suchauftrag besteht:
	end else if (searching) begin
		//$display("     ...searching (checking floor %d, req%d)", checkFloor, ((checkFloor*2)+direction));
		
		// Wenn keinerlei Zielwahltasten gesetzt: Suchauftrag l�schen
		if ((DESTINATIONS==0) && (FLOOR_REQUEST==0)) begin
				searching<=0;
		
		// �u�ere und innere Zielwahltasten abfragen
		end else if ((DESTINATIONS[checkFloor]) || (FLOOR_REQUEST[((checkFloor*2)+direction)])) begin
			// Treffer: Suchauftrag l�schen und Ausgabeparameter NEXT_FLOOR setzen
			searching<=0;
			NEXT_FLOOR<=checkFloor;
			
		end else if ((checkFloor == 0) || (checkFloor == FLOORS-1)) begin
			// Wenn an Ober- oder Untergrenze angekommen, Richtung wechseln
			direction=!direction;
			checkFloor<=checkFloor+(direction?1:-1);
			
		end else begin
			// N�chstes zu pr�fendes Stockwerk anhand gespeicherter Richtung
			checkFloor<=checkFloor+(direction?1:-1);
		end;
	end;// else if searching
	
end;



/* ====================================================================================*/

endmodule
