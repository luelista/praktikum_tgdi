// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 101
// 2527455: Moritz Willig
// 2940980: Maximilian Weller
// ----------------
//
// Description:
// ----------------
// elevator cabin-control
//
// Version History:
// ----------------
// 140116: set parameter FLOOR_BITS to 7

`timescale 1ns / 1ns

module cabin_ctrl

          #(parameter FLOOR_BITS             =        7, // 
                      CONTROL_STEP_DISTANCE  =       10, // [mm]
                      DISTANCE_BITS_BUILDING =       19, // 297.000 mm
                      DISTANCE_BITS_FLOORS   =       12, // 12 bits <> 3000mm, required for height counter
                      FLOOR_HEIGHT           =     3000, // [mm]
                      WAIT_CYCLE             = 40000000, // wait 40 Mio cycles for a delay of 2 seconds @ 20 MHz 
                      WAIT_CYCLE_BITS        =       26) // requires 26 bits


          (input  wire                                CLK,
           input  wire                                RESET,
                                                       
           input  wire                                MANUAL_DOOR_CLOSE,     // close
           input  wire                                MANUAL_DOOR_OPEN,      // open
           input  wire                                MANUAL_ALARM,          // alarm
           input  wire                                OBJECT_DETECTED,       // object
           input  wire                                SPEED_OK    ,          // accel_ok
           input  wire                                WEIGHT_OK,             // weight_ok
           input  wire                                SMOKE_DETECTED,        // alarm
           input  wire [(FLOOR_BITS-1)            :0] NEXT_FLOOR,            // next floor to visit
           input  wire                                DOOR_MOTOR_DONE,       // done 
           input  wire                                ELEVATOR_MOTOR_DONE,   // done
           input  wire                                ELEVATOR_MOTOR_TICK,   // Elevatormotor drove 10mm
                      
           output reg  [(FLOOR_BITS-1)            :0] CURRENT_FLOOR,
           output reg                                 HALTED,                // halted at floor
           output wire                                OPEN_DOOR,             // open
           output wire                                CLOSE_DOOR,            // close
           output wire                                ELEVATOR_UP,           // up
           output wire                                ELEVATOR_DOWN,         // down
           output reg  [(DISTANCE_BITS_BUILDING-1):0] DISTANCE,              // go distance in milimeter
           output reg                                 EMERGENCY_BRAKE);      // emergency break cmd

/* =============================INSERT CODE HERE======================================*/ 

	// Definition und Benennung der Zust�nde f�r endlichen Zustandsautomaten
	localparam SRESET = 4'd0;
	localparam SWAITCLOSE = 4'd1;
	localparam SCLOSEDOOR = 4'd2;
	localparam SCLOSINGDOOR = 4'd3;
	localparam SSTOPDOOR = 4'd4;
	localparam SSTOPPINGDOOR = 4'd5;
	localparam SOPENDOOR = 4'd6;
	localparam SOPENINGDOOR = 4'd7;
	localparam SSTARTMOTOR = 4'd8;
	localparam SDRIVING = 4'd9;
	localparam SEMERGBRAKE = 4'd10;
	localparam SCALCDIST = 4'd11;
	
	// 1-bit Z�hlvariable f�r Signale an Motor-Interface (mif)
	reg mif_double, next_mif_double;
	
	// aktuell angepeiltes Zielstockwerk
	reg [(FLOOR_BITS-1)            :0] target_floor;
	
	// Fahrtrichtung: 1 = auf, 0 = ab
	wire direction;
	assign direction = NEXT_FLOOR > CURRENT_FLOOR;
	
	// Zustandsregister des endlichen Zustandsautomaten
	reg [3:0] state, next_state;
	
	always @ (posedge CLK, posedge RESET) begin
		// Zustandsregister des endlichen Zustandsautomaten �bernehmen oder r�ckstellen
		if (RESET) state <= SRESET;
		else state <= next_state;
		
		// 1-bit Z�hlvariable f�r Signale an Motor-Interface (mif) �bernehmen oder r�ckstellen
		if (RESET) mif_double <= 0;
		else mif_double <= next_mif_double;
		
		// Signal f�r Notbremsung �bernehmen oder r�ckstellen
		if (RESET) EMERGENCY_BRAKE <= 0;
		else if (next_state == SEMERGBRAKE) EMERGENCY_BRAKE <= 1;
	end
	
	always @ (posedge CLK) begin
		// Wenn n�chster Zustand SCALCDIST...
		if (next_state == SCALCDIST) begin
			// aktuell angepeiltes Zielstockwerk aus next_floor_ctrl �bernehmen
			target_floor <= NEXT_FLOOR;
			
			// Berechnung der Distanz
			if (direction) begin    // = NEXT_FLOOR > CURRENT_FLOOR;
				DISTANCE <= (NEXT_FLOOR - CURRENT_FLOOR) * FLOOR_HEIGHT;
			end else begin
				DISTANCE <= (CURRENT_FLOOR - NEXT_FLOOR) * FLOOR_HEIGHT;
			end
		end
	end
	
	// Ausg�nge des endlichen Zustandsautomaten berechnen
	assign OPEN_DOOR = (state == SOPENDOOR) || (state == SSTOPDOOR);
	assign CLOSE_DOOR = (state == SCLOSEDOOR) || (state == SSTOPDOOR);
	assign ELEVATOR_UP = (state == SSTARTMOTOR) && direction;
	assign ELEVATOR_DOWN = (state == SSTARTMOTOR) && !direction;
	
	always @ (*) begin
		// Defaultverhalten: Zustand wird nicht ver�ndert
		next_state = state;
		
		//OPEN_DOOR = 0; CLOSE_DOOR = 0; ELEVATOR_UP = 0; ELEVATOR_DOWN = 0;

		// Defaultverhalten: Fahrstuhl befindet sich im Haltezustand 
		HALTED = 1;
		
		case (state)
			// Startzustand. Wenn ein Fahrtauftrag vorhanden ist, und keine Sensorausgaben dagegen sprechen, wird SWAITCLOSE begonnen.
			SRESET: if (NEXT_FLOOR != CURRENT_FLOOR && WEIGHT_OK && !SMOKE_DETECTED && !OBJECT_DETECTED) next_state = SWAITCLOSE;
			
			// Warten auf Beginn des Schlie�ens der T�r (2 sek). Kann vom Bediener durch MANUAL_DOOR_CLOSE beschleunigt werden. Dann, 
			// oder nach Ablauf der Wartezeit, wird das Schlie�en der T�r (SCLOSEDOOR) eingeleitet. Dies kann durch Sensorausgaben 
			// unterbrochen werden -> R�ckkehr zu SRESET
			SWAITCLOSE: begin
				if (wait_counter == WAIT_CYCLE && !MANUAL_ALARM) next_state = SCLOSEDOOR;
				if (MANUAL_DOOR_CLOSE) next_state = SCLOSEDOOR;
				if (MANUAL_DOOR_OPEN || OBJECT_DETECTED || SMOKE_DETECTED || !WEIGHT_OK) next_state = SRESET;
			end
			
			// Schlie�en der T�r. CLOSE_DOOR muss zwei Takte auf eins gehalten werden, danach unbedingt SCLOSINGDOOR
			SCLOSEDOOR: begin
				//CLOSE_DOOR = 1;
				if (mif_double == 1) begin next_state = SCLOSINGDOOR; next_mif_double = 0; end
				else next_mif_double = 1;
			end
			
			// Warten auf Abschluss des Schlie�ens der T�r. Sensorausgaben k�nnen dies unterbrechen und f�hren zum sofortigen Stopp
			// der T�r (SSTOPDOOR). Ansonsten wird bei DOOR_MOTOR_DONE zu SCALCDIST gewechselt.
			SCLOSINGDOOR: begin
				if (MANUAL_DOOR_OPEN || MANUAL_ALARM || OBJECT_DETECTED || SMOKE_DETECTED || !WEIGHT_OK) next_state = SSTOPDOOR;
				if (DOOR_MOTOR_DONE) next_state = SCALCDIST;
			end
			
			// Sofortiger Stop des Schlie�ens der T�r. OPEN_DOOR und CLOSE_DOOR m�ssen zwei Takte auf eins gehalten werden, 
			// danach unbedingt SSTOPPINGDOOR
			SSTOPDOOR: begin
				//OPEN_DOOR = 1; CLOSE_DOOR = 1;
				if (mif_double == 1) begin next_state = SSTOPPINGDOOR; next_mif_double = 0; end
				else next_mif_double = 1;
			end
			
			// Warten auf Stoppen der T�r (2 sec), danach unbedingt SOPENDOOR
			SSTOPPINGDOOR:
				if (wait_counter == WAIT_CYCLE) next_state = SOPENDOOR;
			
			// �ffnen der T�r. OPEN_DOOR muss zwei Takte auf eins gehalten werden, danach unbedingt SCLOSINGDOOR
			SOPENDOOR: begin
				//OPEN_DOOR = 1;
				if (mif_double == 1) begin next_state = SOPENINGDOOR; next_mif_double = 0; end
				else next_mif_double = 1;
			end
			
			// Warten auf Abschluss des �ffnens der T�r. Bei DOOR_MOTOR_DONE wird zu SRESET gewechselt.
			SOPENINGDOOR: begin
				if (DOOR_MOTOR_DONE) next_state = SRESET;
			end
			
			// Notbremsung. Dieser Zustand stellt eine Sackgasse dar und kann nur durch RESET verlassen werden.
			SEMERGBRAKE: begin end
			
			
			// ...in den folgenden drei Zust�nden befindet sich der Fahrstuhl nicht im Haltezustand
			
			// Berechnen der Fahrtdistanz. Danach unbedingt SSTARTMOTOR.
			SCALCDIST: begin
				HALTED = 0;
				next_state = SSTARTMOTOR;
			end
			
			// Starten des Aufzugsmotors. Abh�ngig von der gew�nschten Fahrtrichtung muss entweder ELEVATOR_UP oder
			// ELEVATOR_DOWN zwei Takte auf eins gehalten werden, danach unbedingt SDRIVING.
			SSTARTMOTOR: begin
				HALTED = 0;
				if (direction) begin    // = NEXT_FLOOR > CURRENT_FLOOR;
					//ELEVATOR_UP = 1;
				end else begin
					//ELEVATOR_DOWN = 1;
				end
				if (mif_double == 1) begin next_state = SDRIVING; next_mif_double = 0; end
				else next_mif_double = 1;
			end
			
			// Fahrt des Fahrstuhls. Bei ELEVATOR_MOTOR_DONE wird zu SOPENDOOR gewechselt. Bei rechtzeitiger �nderung 
			// des Zielstockwerkes wird zu SCALCDIST gewechselt. Bei �berschreitung der Geschwindigkeit wird eine Not-
			// bremsung eingeleitet.
			SDRIVING: begin
				HALTED = 0;
				if (ELEVATOR_MOTOR_DONE) next_state = SOPENDOOR;
				if ((ctrl_step_counter == 0) && (NEXT_FLOOR != target_floor) 
					&& (target_floor != CURRENT_FLOOR) 
					&& (target_floor + 1 != CURRENT_FLOOR) 
					&& (target_floor - 1 != CURRENT_FLOOR)) next_state = SCALCDIST;
				if (!SPEED_OK) next_state = SEMERGBRAKE;
			end
			
		endcase
	end
	
	
	// WAIT TIMER
	
	reg [WAIT_CYCLE_BITS-1:0] wait_counter;
	wire wait_reset;
	
	// au�er in den Zust�nden SWAITCLOSE und SSTOPPINGDOOR wird der WAIT TIMER 
	// immer im Reset-Zustand gehalten
	assign wait_reset = (state != SWAITCLOSE && state != SSTOPPINGDOOR);
	
	always @ (posedge CLK) begin
		if (wait_reset) begin
			wait_counter = 0;
		
		// der WAIT TIMER h�lt an, sobald er den Maximalwert erreicht hat
		end else	if (wait_counter != WAIT_CYCLE) begin
			wait_counter = wait_counter + 1;
		end
	end
	
	
	// CONTROL STEP COUNTER
	
	reg [(DISTANCE_BITS_FLOORS-1):0] ctrl_step_counter;
	reg motor_tick_rem;
	
	always @ (posedge CLK, posedge RESET) begin
		if (RESET) begin
			CURRENT_FLOOR <= 0;
			ctrl_step_counter = 0;
		end else begin
			// der CONTROL STEP COUNTER wird um die CONTROL_STEP_DISTANCE er-
			// h�ht, sobald im aktuellen Takt MOTOR_TICK gesetzt ist und im
			// vorherigen Takt nicht gesetzt war
			if (ELEVATOR_MOTOR_TICK && !motor_tick_rem) 
				ctrl_step_counter = ctrl_step_counter + CONTROL_STEP_DISTANCE;
			motor_tick_rem <= ELEVATOR_MOTOR_TICK;
			
			// wenn die H�he eines Stockwerkes abgez�hlt ist
			if (ctrl_step_counter == FLOOR_HEIGHT) begin
				// wird CURRENT_FLOOR abh�ngig von der direction inkrementiert
				if (direction) CURRENT_FLOOR <= CURRENT_FLOOR + 1;
				// oder dekrementiert
				else CURRENT_FLOOR <= CURRENT_FLOOR - 1;
				// sowie der CONTROL STEP COUNTER r�ckgestellt
				ctrl_step_counter = 0;
			end
		end
	end
	




/* ====================================================================================*/ 

endmodule
