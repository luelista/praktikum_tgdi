// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 101
// 2527455: Moritz Willig
// 2940980: Maximilian Weller
// ----------------
//
// Description:
// ----------------
// elevator keyboard-interface
//
// Version History:
// ----------------
// 140116: set parameter FLOORS to 30
// 140116: set parameter FLOOR_BITS to 5

`timescale 1ns / 1ns

module keyboard_if

          #(parameter FLOORS     = 30,
                      FLOOR_BITS =  5)


          (input  wire                    CLK,
           input  wire                    RESET,
           
           input  wire [(FLOORS-1)    :0] FLOOR_SELECT,             
           input  wire                    CLOSE_DOOR_IN,            
           input  wire                    OPEN_DOOR_IN,             
           input  wire                    PASSENGER_ALARM_IN,       
           input  wire [(FLOOR_BITS-1):0] CLEAR_FLOOR_BUTTON,       // reset_button
           input  wire                    CLEAR_FLOOR_BUTTON_VALID, 
            
           output wire                    CLOSE_DOOR_OUT,           // close
	   output wire			  OPEN_DOOR_OUT,	    // open
           output wire                    PASSENGER_ALARM_OUT,      // alarm        
           output wire [(FLOORS-1)    :0] SELECTED_FLOORS,          // floor
           output wire [(FLOORS-1)    :0] ENLIGHT_BUTTONS);         // enlight      
     
/* =============================INSERT CODE HERE======================================*/ 
	
	// Zwischespeicher der Stockwerk-Taster
	reg[(FLOORS-1)    :0] floor_reg;

	// speichern und weiterreichen der Eingaben über Stockwerk-Taster
	always @ (posedge CLK) begin
		if (RESET) begin //vollständiger Reset
			floor_reg = 0;
		end
		floor_reg = floor_reg | FLOOR_SELECT; // auswahl speichern
		
		if (CLEAR_FLOOR_BUTTON_VALID) // einzelnen Reset auswerten
						// Binärcodierung in One-Hot-Codierung umwandeln und maskieren
			floor_reg = floor_reg & (~ (1 << CLEAR_FLOOR_BUTTON)); 
		
	end
	
	assign ENLIGHT_BUTTONS = floor_reg; //ausgabe auf kontrollleuchten
	assign SELECTED_FLOORS = floor_reg; //...und an controller
	
	// Übertragen der Taster-Eingaben Auf, Zu, Alarm
	assign CLOSE_DOOR_OUT = CLOSE_DOOR_IN;
	assign OPEN_DOOR_OUT = OPEN_DOOR_IN;
	assign PASSENGER_ALARM_OUT = PASSENGER_ALARM_IN;


/* ====================================================================================*/

endmodule
